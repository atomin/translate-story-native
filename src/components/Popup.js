import React from 'react'

export default function Popup(props) {
  const { closePopup, children } = props

  function click(e) {
    if (e.currentTarget === e.target) {
      // This means user clicked to the gray background area, not inside the overlay children
      closePopup()
    }
  }

  return (
    <div className="popup-background" onClick={click}>
      {children}
    </div>
  )
}
