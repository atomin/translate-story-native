import React, { Component } from 'react'

export default class WordTranslation extends Component {
  render() {
    const { wordTranslation: { coords, translation } } = this.props

    return (
      <div className="word-popup">
        {translation.map((t, ind) =>
          <div key={ind}>
            {t.text}
            <span className="transcription">
              {t.ts && `  [${t.ts}]`}
            </span>
            <span className="position">
              {`  ${t.pos.tooltip}`}
            </span>
            {t.tr.map((t1, ind1) =>
              <div className="translation-item" key={ind1}>
                <span className="index">
                  {`${ind1 + 1}  `}
                </span>
                <span className="translation">
                  {[t1.text, ...(t1.syn || []).map(x => x.text)].join(', ')}
                </span>
                <div className="means">
                  {(t1.mean || []).map(x => x.text).join(', ')}
                </div>
                <div className="examples">
                  {(t1.ex || []).map((x, ind2) =>
                    <div key={ind2}>
                      <span>{x.text}</span> -{' '}
                      <span>{(x.tr || []).map(y => y.text).join(', ')}</span>
                    </div>
                  )}
                </div>
              </div>
            )}
          </div>
        )}
      </div>
    )
  }
}
