import React from 'react'

import { StyleSheet, Text, View, TextInput, FlatList } from 'react-native'

function getOriginChunk(chunk, { word, vissible }) {
  if (!vissible) {
    return chunk
  }
  const parts = chunk
    .split(
      new RegExp(`(?:[ \n\r\t.,\\'"+!?-]+|^)${word}(?=[ \n\r\t.,\\'"+!?-]+|$)`)
    )
    .map((x, ind) => [
      <span key={ind}>
        {x}
      </span>,
      <span key={`${ind}-selected`} className="selected-word">
        {word}
      </span>
    ])

  const flatten = [].concat.apply([], parts)
  flatten.length--

  return flatten
}

function TranslatedText(props) {
  const {
    originChunks,
    translatedChunks,
    fontSize,
    onOriginClick,
    wordTranslation
  } = props
  const listData = originChunks.map((item, ind) => [item, translatedChunks[ind]])
  return (
    <View style={styles.container}>
      <FlatList
        data={listData}
        keyExtractor={(item, index) => index}
        renderItem={({ item, index }) =>
          <View>
            <Text style={styles.item}>
              {item[0]}
            </Text>
            <Text style={styles.translatedItem}>
              {item[1]}
            </Text>
          </View>}
      />
    </View>
  )

  return (
    <article className="translated-text">
      {originChunks.map((chunk, ind) =>
        <div key={`${ind}-chunk`} className="chunks-pair" style={{ fontSize }}>
          <p className="chunk origin-chunk" onClick={onOriginClick}>
            {getOriginChunk(chunk, wordTranslation)}
          </p>
          <p className="chunk translated-chunk">
            {translatedChunks[ind]}
          </p>
        </div>
      )}
      {!!translatedChunks.length &&
        <div className="yandex">
          <a href="http://translate.yandex.com/" target="about:blank">
            Powered by Yandex.Translate
          </a>
        </div>}
    </article>
  )
}

const styles = StyleSheet.create({
  item: {},
  translatedItem: {
    backgroundColor: '#edf0f3',
  },
  container: {}
})

export default TranslatedText
