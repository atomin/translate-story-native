import React, { Component } from 'react'

import storage from '../helpers/storage'

const SHORT_TEXT_LENGTH = 100

class History extends Component {
  constructor(...args) {
    super(...args)
    this.state = { history: [] }
  }

  componentDidMount() {
    const history = storage.getHistory().map(text => ({
      text,
      short: text.substr(0, SHORT_TEXT_LENGTH)
    }))
    this.setState({ history })
  }

  render() {
    const { history } = this.state
    const { setFromHistory } = this.props

    return (
      <div className="history">
        <div className="history-items">
          {history.map((item, ind) =>
            <div
              key={ind}
              className="history-item"
              onClick={() => setFromHistory(item.text)}
            >
              {item.short}
              <span className="text-lengh">
                ({item.text.length} chars)
              </span>
            </div>
          )}
        </div>
      </div>
    )
  }
}

export default History
