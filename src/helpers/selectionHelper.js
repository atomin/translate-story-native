function getSelectionCoords(win) {
  let x = 0
  let y = 0
  return { x, y }
}

const NON_WORD_REGEXP = /[ \n\r\t.,\\'"+!?-]+/

export default function selectWord(e) {
  return {
    word: '',
    coords: getSelectionCoords()
  }
}
