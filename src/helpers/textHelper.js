const separators = /(\.+|\?|!|;)/

export function split(text = '', chunkSize = 300) {
  const sentenses = text
    .split(separators)
    .map(s => s.trim())
    .reduce((res, cur, ind) => {
      if (ind % 2) {
        res[res.length - 1] += cur
      } else {
        res.push(cur)
      }
      return res
    }, [])
    .reduce((res, cur) => {
      if (res.length === 0) {
        return [cur]
      }
      const curLength = res[res.length - 1].length
      const curDelta = curLength - chunkSize
      const nextDelta = curLength + cur.length - chunkSize
      if (Math.abs(curDelta) >= Math.abs(nextDelta)) {
        res[res.length - 1] += cur
      } else {
        res.push(cur)
      }
      return res
    }, [])
    .filter(s => s.length)

  return sentenses.length ? sentenses : ['']
}
