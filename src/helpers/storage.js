const localStorage = {
  getItem() {
    return null
  },

  setItem() {}
}

export default {
  getLanguage() {
    return localStorage.getItem('language')
  },

  setLanguage(lang) {
    localStorage.setItem('language', lang)

    const langs = JSON.parse(localStorage.getItem('languages') || '[]')
    const newLangs = [lang, ...langs.filter(x => x !== lang)]
    localStorage.setItem('languages', JSON.stringify(newLangs))
  },

  getLanguages() {
    return JSON.parse(localStorage.getItem('languages') || '[]')
  },

  getHistory() {
    return JSON.parse(localStorage.getItem('history') || '[]')
  },

  addToHistory(text) {
    let history: Array = this.getHistory()
    const newHistory = [text, ...history.filter(x => x !== text)]
    localStorage.setItem('history', JSON.stringify(newHistory))
  },

  setChunkSize(chunkSize) {
    localStorage.setItem('chunkSize', chunkSize)
  },

  getChunkSize(defaultValue) {
    const chunkSizeStr = localStorage.getItem('chunkSize')
    return (chunkSizeStr && parseInt(chunkSizeStr, 10)) || defaultValue
  },

  setFontSize(fontSize) {
    localStorage.setItem('fontSize', fontSize)
  },

  getFontSize(defaultValue) {
    const fontSizeStr = localStorage.getItem('fontSize')
    return (fontSizeStr && parseInt(fontSizeStr, 10)) || defaultValue
  },

  getNightMode() {
    const nightMode = localStorage.getItem('nightMode')
    return nightMode === 'true'
  },

  setNightMode(nightMode) {
    localStorage.setItem('nightMode', nightMode)
  }
}
