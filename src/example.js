const example = `Capítulo 1.
Dorothy vivía en las grandes praderas de Kansas con
su tío Henry y la tía Em – su mujer.
Ellos eran granjeros y vivían en una casa muy pequeña.
La casa tenía sólo una habitación y no había muchas cosas en ella. En la casa había una mesa, un armario, tres o cuatro sillas y dos camas.
La única ventana daba a un pequeño jardín. Pero cuando Dorothy miraba por la ventana no veía ni árboles ni flores cerca de la casa. Ni siquiera la hierba era verde sino gris. Todo en sus vidas era de color gris, todo era muy triste.
El tío Henry y la tía Em trabajaban todo el día. Empezaban muy temprano por la mañana, paraban tarde por la noche y nunca se reían.
A pesar de esto, Dorothy era feliz porque tenía un amigo.
Su amigo era un perro de color negro llamado Totó. Él tenía pelo largo, ojos brillantes y cola corta.
El perro y la niña corrían y jugaban todos los días. Eran los mejores amigos y se entendían uno al otro sin palabras.
“El ciclón”
“Un ciclón, Em, viene un ciclón!”
Un mediodía pasó algo muy extraño. El tío Henry y Dorothy, con Totó en brazos, estaban en el patio. El tío Henry miró al cielo desde la puerta de la casa.
“Mira que está pasando”, le dijo a Dorothy.
Dorothy vio oscurecerse el cielo, que ya no era azul. Ella se asustó con lo que había visto.
De repente empezó una tormenta. El tío Henry le gritó a la tía Em, que estaba en la casa: “Un ciclón, Em, viene un ciclón!”
Capítulo “El ciclón”.
El ciclón levantó la pequeña casa en el aire.
La tía Em salió corriendo para proteger a las vacas y a los caballos.
“Deprisa, Dorothy”, dijo ella: “Ve a la casa y quédate allí. Tengo que ayudar al tío Henry”.
Dorothy estaba muy asustada y corrió hacia la casa. Totó saltó de sus brazos y se escondió debajo de la
cama. Él también tenía miedo.
Algo terrible pasó en ese momento:
El ciclón levantó la pequeña casa en el aire y luego se la llevó muy lejos.
La casa se alejaba muy rápidamente y Dorothy, que no sabía que hacer, se puso a llorar.
Pero el tiempo pasaba, minuto tras minuto, hora tras hora y finalmente se quedó dormida. ...
La historia continua en el capitulo 2 ...`

export default example
